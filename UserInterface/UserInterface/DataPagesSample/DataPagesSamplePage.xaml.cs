﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserInterface.DataPagesSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DataPagesSamplePage : ContentPage
    {
        public DataPagesSamplePage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GettingStarted.GettingStartedSamplePage());
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CustomDataPageSamplePage());
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {

        }
    }
}