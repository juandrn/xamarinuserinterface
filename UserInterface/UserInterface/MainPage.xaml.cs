﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.DataPagesSample.ImagesSample;
using UserInterface.LayoutSample;
using Xamarin.Forms;

namespace UserInterface
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Animation_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AnimationSample.AnimationSamplePage());
        }


        private void DataPages_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataPagesSample.DataPagesSamplePage());
        }

        private void Images_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ImageSamplePage());
        }

        private void Layouts_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LayoutSamplePage());
        }

        private void ListView_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListViewSample.ListViewSamplePage());
        }

        private void Picker_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PickerSample.PickerSamplePage());
        }

        private void Styles_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new StyleSample.StyleSamplePage());
        }

        private void Menu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TableViewSample.TableViewSamplePage());
        }

        private void Theme_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ThemeSample.ThemeSamplePage());
        }

        private void VSM_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new VSMSample.VSMSamplePage());
        }

        private void WebView_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new WebViewSample.WebViewSamplePage());
        }
    }
}