﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserInterface.AnimationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EasingFunctionsSamplePage : ContentPage
    {
        public EasingFunctionsSamplePage()
        {
            InitializeComponent();
        }

        private void StopAnimation_Clicked(object sender, EventArgs e)
        {
            ViewExtensions.CancelAnimations(image);
            image.Rotation = 0;
            this.image.ScaleTo(1, 2000);
            this.image.Opacity = 1;
            this.TranslateTo(0, 0, 100, Easing.CubicOut);
        }

        private async void Rotate_Clicked(object sender, EventArgs e)
        {
            await this.image.RotateTo(180, 1000, Easing.SinIn);///Acelera suavemente
            await this.image.RotateTo(-180, 1000, Easing.SinOut);///Desacelera suavemente
        }



        private async void Scale_Clicked(object sender, EventArgs e)
        {
            await image.ScaleTo(2, 2000, Easing.CubicIn);
            await image.ScaleTo(1, 2000, Easing.CubicOut);
        }

        private async void MoverConTodo_Clicked(object sender, EventArgs e)
        {
            await image.TranslateTo(0, 200, 2000, Easing.BounceIn);
            await image.ScaleTo(2, 2000, Easing.CubicIn);
            await image.RotateTo(360, 2000, Easing.SinInOut);
            await image.ScaleTo(1, 2000, Easing.CubicOut);
            await image.TranslateTo(0, -200, 2000, Easing.BounceOut);
        }

        private async void CustomEase_Clicked(object sender, EventArgs e)
        {
            Func<double, double> CustomEase = t => 9 * t * t * t - 13.5 * t * t + 5.5 * t;
            await image.TranslateTo(0, 200, 1000, CustomEase);
            await image.TranslateTo(0, 0, 200, new Easing(t => 1 - Math.Cos(10 * Math.PI * t) * Math.Exp(-5 * t)));
        }
    }
}