﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserInterface.AnimationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomAnimationsSamplePage : ContentPage
    {
        public CustomAnimationsSamplePage()
        {
            InitializeComponent();
        }

        private void SimpleAnimation_Clicked(object sender, EventArgs e)
        {
            var animation = new Animation(v => image.Scale = v, 1, 2);
            animation.Commit(this, "SimpleAnimation", 16, 2000, Easing.Linear, (v, c) => image.Scale = 1, () => true);
        }

        private void AnimationWithChild_Clicked(object sender, EventArgs e)
        {

        }
    }
}