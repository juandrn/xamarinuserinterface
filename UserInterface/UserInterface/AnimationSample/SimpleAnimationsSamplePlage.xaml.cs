﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserInterface.AnimationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleAnimationsSamplePlage : ContentPage
    {
        public SimpleAnimationsSamplePlage()
        {
            InitializeComponent();
        }


        private void StopAnimation_Clicked(object sender, EventArgs e)
        {
            ViewExtensions.CancelAnimations(image);
            image.Rotation = 0;
            this.image.ScaleTo(1, 2000);
            this.image.Opacity = 1;
        }

        private void Rotate_Clicked(object sender, EventArgs e)
        {
            this.image.RotateTo(360, 2000);
        }

        private void Opacity_Clicked(object sender, EventArgs e)
        {
            this.image.FadeTo(0.5, 3000);
        }

        private void Scale_Clicked(object sender, EventArgs e)
        {
            this.image.ScaleTo(2, 2000);
        }

        private async void MoverConTodo_Clicked(object sender, EventArgs e)
        {
            this.image.ScaleTo(0.5, 2000);
            this.image.FadeTo(0.5, 2000);
            this.image.RotateTo(180, 1000);
            await image.TranslateTo(-100, 0, 1000);    // Move image left
            this.image.RotateTo(180, 1000);
            this.image.FadeTo(1, 1000);
            await image.TranslateTo(-100, -100, 1000); // Move image up
            this.image.RotateTo(-180, 1000);
            this.image.FadeTo(0.5, 1000);
            await image.TranslateTo(100, 100, 1000);   // Move image diagonally down and right
            this.image.RotateTo(-180, 1000);
            await image.TranslateTo(0, 100, 1000);     // Move image left
            this.image.RotateTo(180, 1000);
            await image.TranslateTo(0, 0, 1000);       // Move image up
            this.image.RotateTo(180, 1000);
        }
    }
}