﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserInterface.AnimationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimationSamplePage : ContentPage
    {
        public AnimationSamplePage()
        {
            InitializeComponent();
        }

        private void SimpleAnimationsSamplePage_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SimpleAnimationsSamplePlage());
        }

        private void EasingFunctions_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EasingFunctionsSamplePage());
        }

        private void CustomAnimations_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CustomAnimationsSamplePage());
        }
    }
}